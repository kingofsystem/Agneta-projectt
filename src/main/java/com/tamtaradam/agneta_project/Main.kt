package com.tamtaradam.agneta_project

import com.jme3.animation.AnimControl
import com.jme3.animation.LoopMode
import com.jme3.app.SimpleApplication
import com.jme3.audio.AudioNode
import com.jme3.cinematic.MotionPath
import com.jme3.collision.CollisionResults
import com.jme3.input.MouseInput
import com.jme3.input.controls.ActionListener
import com.jme3.input.controls.MouseButtonTrigger
import com.jme3.light.PointLight
import com.jme3.material.Material
import com.jme3.math.*
import com.jme3.scene.Geometry
import com.jme3.scene.Node
import com.jme3.scene.plugins.blender.BlenderLoader
import com.jme3.scene.shape.Sphere
import com.jme3.water.SimpleWaterProcessor
import com.sun.org.apache.regexp.internal.RE
import java.util.*

/**
 * Маленькая игра для Полянского Сергея.
 * Будет продоваться вместе с его интерактивной доской.
 * Created by Vadim Zalyubovskiy aka agneta_project.tamtaradam on 03.07.16.
 */

const val ANIM_CTRL = "animCtrl"
const val SWIM_VEC = "swimVector"
const val ROTATION_DELTA = "rotateVec"
const val RETURN_POINT = "returnPoint"
const val RETURNING = "returning"
const val FISH_ROT = "fishRot"

val DEFAULT_SWIM_VEC: Vector3f
get() = Vector3f(1f, 0f, 0f)

val DEFAULT_ROTATION: FloatArray = floatArrayOf(0f, 20f * FastMath.DEG_TO_RAD, 0f)

const val FISH_COUNT = 40

const val SPEED_UP = 5f

fun main(args: Array<String>) {
    Main().start()
}

class Main: SimpleApplication() {

    private val fishNode  = Node("fish_node")
    private lateinit var water: Geometry

    override fun simpleInitApp() {
        assetManager.registerLoader(BlenderLoader::class.java, "blend")

        val simpleScene = assetManager.loadModel("simple_scene.blend") as Node
        rootNode.attachChild(simpleScene)

        repeat(FISH_COUNT, {spawnfish()})
        rootNode.attachChild(fishNode)

        // Add lighting
        val dl = PointLight()
        dl.color = ColorRGBA.White
        dl.radius = 100f
        dl.position = Vector3f(0f, 10f, 0f)
        rootNode.addLight(dl)

        val waterProcessor = SimpleWaterProcessor(assetManager)
        waterProcessor.setReflectionScene(rootNode)
        waterProcessor.waterTransparency = Float.MAX_VALUE
        waterProcessor.setRenderSize(1024, 1024)
        waterProcessor.waterDepth = 200f
        waterProcessor.setWaterColor(ColorRGBA.Blue)
        waterProcessor.setLightPosition(dl.position)
        waterProcessor.texScale = 2f
        waterProcessor.waveSpeed = 0.03f
        waterProcessor.distortionScale = 0.05f
        waterProcessor.distortionMix = .1f
        viewPort.addProcessor(waterProcessor)

        water = waterProcessor.createWaterGeometry(10f, 10f)
        water.material = waterProcessor.material
        water.localTranslation = Vector3f(-4f, 1f, 5f)
        rootNode.attachChild(water)

        cam.location = Vector3f(0f, 5f, -2f)
        cam.rotation = Quaternion(floatArrayOf(FastMath.DEG_TO_RAD * 90f, 0f , 0f))

        initInput()
        initAudio()
        runMaskReceiver(Pair(cam.width, cam.height), maskCallback)
    }

    private fun initAudio() {
        val water_sound = AudioNode(assetManager, "/audio/water.ogg", false)
        water_sound.isPositional = false
        water_sound.isLooping = true
        water_sound.volume = 1f
        water_sound.play()
        rootNode.attachChild(water_sound)
    }

    val maskCallback = fun(points: LinkedList<Pair<Int, Int>>) {
        for (point in points) {
            val point2d = Vector2f(point.first.toFloat(), point.second.toFloat())
            val point3d = cam.getWorldCoordinates(point2d, 0f)
            val dir = cam.getWorldCoordinates(point2d, 0.3f).subtractLocal(point3d).normalizeLocal()

            val collResult = CollisionResults()
            val ray = Ray(point3d, dir)
            fishNode.collideWith(ray, collResult)

            if (collResult.size() == 0) {
                return
            }

            val contactNode = collResult.closestCollision.geometry.parent.parent
            contactNode.setUserData(RETURN_POINT, contactNode.worldTranslation.clone())
            contactNode.setUserData(RETURNING, false)
            contactNode.setUserData(SWIM_VEC, DEFAULT_SWIM_VEC.mult(SPEED_UP))
            contactNode.setUserData(ROTATION_DELTA, floatArrayOf(0f, 0f, 0f).toList())
            contactNode.setAnimSpeed(2f)
        }
    }

    private fun initInput() {
        flyCam.isEnabled = false
        inputManager.isCursorVisible = true

        inputManager.addMapping("click", MouseButtonTrigger(MouseInput.BUTTON_LEFT))
        inputManager.addListener(clickListener, "click")
    }

    val clickListener = ActionListener { name, keyPressed, tpf ->
        if (name.equals("click") and !keyPressed) {
            println("Start colliding!")
            val click2d = inputManager.cursorPosition
            val testList = LinkedList<Pair<Int, Int>>()
            testList.add(Pair(click2d.x.toInt(), click2d.y.toInt()))
            maskCallback(testList)
        }
    }

    private fun _create_sphere(location: Vector3f): Geometry {
        val sphere = Sphere(10, 10, .2f)
        val sphereGeo = Geometry("sphere", sphere)
        val sphereMat = Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
        sphereMat.setColor("Color", ColorRGBA.Blue)
        sphereGeo.material = sphereMat
        sphereGeo.localTranslation = location
        rootNode.attachChild(sphereGeo)
        return sphereGeo
    }

    private fun spawnfish() {
        val goldFish = loadFish().getChild("gold_fish") as Node
        val fish_mesh = goldFish.getChild("fish_mesh")
        var vec_loc: Vector3f? = null

        whl@while (vec_loc == null) {
            val x = (Math.random() * 10.0 - 5.0).toFloat()
            val z = (Math.random() * 10.0 - 5.0).toFloat()
            val y = (Math.random() * -3.0 - 1.0).toFloat()
            if (fishNode.children.size == 0) {
                vec_loc = Vector3f(x, y, z)
            }
            for (fish in fishNode.children) {
                val dist = Vector3f(x, y, z).add(fish.localTranslation.negate()).length()
                if (dist >= 5) {
                    vec_loc = Vector3f(x, y, z)
                } else {
                    continue@whl
                }
            }
        }

        goldFish.localTranslation = vec_loc
        val fishRot = floatArrayOf(0f, (Math.random()*(FastMath.PI*2)).toFloat(), 0f)
        goldFish.localRotation = Quaternion().fromAngles(fishRot)
        fishNode.attachChild(goldFish)
        goldFish.scale((Math.random()*3+1).toFloat())

        val control = fish_mesh.getControl(AnimControl::class.java)
        val animChan = control.createChannel()
        animChan.loopMode = LoopMode.Loop
        animChan.setAnim("dangle", 1f)

        goldFish.setUserData(ANIM_CTRL, control)
        goldFish.setUserData(SWIM_VEC, DEFAULT_SWIM_VEC)
        goldFish.setUserData(ROTATION_DELTA, DEFAULT_ROTATION.toList())
        goldFish.setUserData(FISH_ROT, fishRot.toList())
    }

    private fun loadFish(): Node {
        return assetManager.loadModel("gold_fish.blend") as Node
    }

    private fun <T: Node> T.setAnimSpeed(speed: Float) {
        val animCtrl: AnimControl = this.getUserData(ANIM_CTRL)
        val animChan = animCtrl.getChannel(0)
        animChan.speed = speed
    }

    override fun simpleUpdate(tpf: Float) {
        for (fish in fishNode.children) {
            val fish = fish as Node

            if (fish.getUserData<Boolean>(RETURNING) != null) {
                val vecSumm = fish.localTranslation.add(fish.getUserData<Vector3f>(RETURN_POINT).negate())
                val returning: Boolean = fish.getUserData(RETURNING)

                if (!returning) {
                    if (vecSumm.length() >= 15f) {
                        fish.setUserData(SWIM_VEC, DEFAULT_SWIM_VEC)
                        fish.setUserData(RETURNING, true)
                        fish.setAnimSpeed(1.5f)
                        fish.rotate(0f, FastMath.PI, 0f)
                        fish.setUserData(FISH_ROT, fish.localRotation.toAngles(null).toList())
                    }
                } else  {
                    if (vecSumm.length() <= .1f) {
                        fish.setUserData(RETURNING, null)
                        fish.setUserData(ROTATION_DELTA, DEFAULT_ROTATION.toList())
                        fish.setAnimSpeed(1f)
                    }
                }
            }

            // we rotating if rotQuat not zero
            var rot: List<Float> = fish.getUserData(FISH_ROT)
            val rotDelta: List<Float> = fish.getUserData(ROTATION_DELTA)
            rot = rot.mapIndexed({i, e -> e + rotDelta[i] * tpf})
            fish.setUserData(FISH_ROT, rot)
            fish.localRotation = Quaternion(rot.toFloatArray())

            // we go to target anyway
            var swimVec: Vector3f = fish.getUserData(SWIM_VEC)
            swimVec = fish.localRotation.mult(swimVec.mult(tpf))
//            println(swimVec)
            fish.move(swimVec)
        }
    }



}