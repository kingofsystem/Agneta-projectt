package com.tamtaradam.agneta_project

import java.io.*
import java.net.Socket
import java.util.*
import kotlin.concurrent.timer

/**
 * Created by Vadim Zalyubovskiy aka agneta_project.tamtaradam on 16.08.16.
 */

const val HOST = "google.ru"
const val PORT = 80

fun runMaskReceiver(resolution: Pair<Int, Int>, callback: (LinkedList<Pair<Int, Int>>) -> Unit){
    val streams = connect(resolution) ?: return
    makeTimer(streams, callback, resolution)
}

private fun connect(resolution: Pair<Int, Int>): Pair<BufferedInputStream, BufferedOutputStream>? {
    try {
        val s = Socket(HOST, PORT)
        val input  = BufferedInputStream(s.inputStream)
        val output = BufferedOutputStream(s.outputStream)

        output.write(resolution.first)
        output.write(resolution.second)

        return Pair(input, output)

    } catch (e: IOException) {
        e.printStackTrace()
        return null
    }
}

private fun makeTimer(streams: Pair<BufferedInputStream, BufferedOutputStream>,
                      callback: (LinkedList<Pair<Int, Int>>) -> Unit,
                      resolution: Pair<Int, Int>) {
    val input = streams.first
    val output = streams.second

    timer(daemon = true, period = 1000.toLong() / 3, action = {
        output.write(byteArrayOf(1.toByte()))
        val len = input.read()
        val buf = ByteArray(len)
        input.read(buf)
        callback(processData(buf, resolution.first))
    })
}

private fun processData(data: ByteArray, xRes: Int): LinkedList<Pair<Int, Int>> {
    val points = LinkedList<Pair<Int, Int>>()
    data.mapIndexed { i, byte ->
        if (byte == 255.toByte()) {
            points.add(Pair(i % (xRes - 1), i / xRes))
        }
    }
    return points
}
